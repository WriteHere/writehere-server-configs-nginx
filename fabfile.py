# -*- coding: UTF-8 -*-
from fabric.api import *

env.hosts = ['wh']
env.use_ssh_config = True
env.deploy_dir = "/etc/nginx"

def pull():
    local("git push")
    with cd(env.deploy_dir):
        run("git pull")

def start():
    run("sudo service nginx start")

def stop():
    run("sudo service nginx stop")

def restart():
    run("sudo service nginx restart")

def reload():
    run("sudo service nginx reload")

def test():
    run("sudo nginx -t")
