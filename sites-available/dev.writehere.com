server {
    listen               80;
    server_name          dev.writehere.com;
    access_log           off;
    error_log            /var/log/nginx/writehere-dev.log;
    client_max_body_size 4m;

    location /sitemap.xml {
        root /srv/src/writehere-dev/writehere.com/app/static/;
    }

    location /robots.txt {
        root /srv/src/writehere-dev/writehere.com/app/static/;
    }

    location /google80643a03168790bc.html {
        root /srv/src/writehere-dev/writehere.com/app/static/;
    }

    location /static {
        alias       /srv/src/writehere-dev/writehere.com/app/static/;
        index       index.html index.htm;
        add_header  Cache-Control "public, max-age=30";
    }

    location / {
        include     uwsgi_params;
        uwsgi_pass  unix:///tmp/writehere-dev.sock;
    }

}
